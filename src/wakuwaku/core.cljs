;; ## Javascript SDK for WakuWaku
;;
;; prototype coded in ClojureScript - doc hosted [here](http://freakhill.bitbucket.org/docs/uberdoc.html)
;;
;; [Differences with Clojure](https://github.com/clojure/clojurescript/wiki/Differences-from-Clojure)
;;
;; Quick tutorial
;;
;; - LISP-1
;; - immutable by default
;; - (atom x) ; mutable something
;; - **(defn f1 ...)** ; define function, Same as (def name (fn [params* ] exprs*)) 
;; - **{ :a 1 }** ; map literal
;; - **(:a {:a 1})** ; will return 1, access to a hashmap
;; - **(defrecord R [fields] ... )** ; immutable record that behaves like a hashmap
;; - **(defprotocol P (f1 [this]) (f2 [this x]))** ; ~Interface
;;
;; Now you can read the code!
;;
;; ps:
;;
;; In order to facilitate a manual JS translation we don't use core.async.
;; We limit ourselves to raw CLJS, Javascript libraries and DOM. (One exception, Dommy for templating).
;; We won't use webworkers either but XmlHttpRequest and iframe tricks!
;;
;; - [Clojure Cheatsheet](http://clojure.org/cheatsheet)
;; - [ClojureScript Cheatsheet](http://appletree.or.kr/quick_reference_cards/Others/ClojureScript%20Cheat%20Sheet.pdf)
;; - [Markdown syntax](http://daringfireball.net/projects/markdown/syntax)
;; - build once: lein cljsbuild once
;; - repeatedly: lein cljsbuild auto
;;
(ns wakuwaku.core
  (:require [weasel.repl :as ws-repl]
            [goog.net.cookies]))

(enable-console-print!)

;; ### API  - publicly exposed (to JS)
;;
;; This is what you will call from javascript with for instance:
;;
;;     goog.require("wakuwaku.core");
;;     wakuwaku.core.activate($("coupons"),
;;                              "MY_COMPANY",
;;                              "THIS_GAME",
;;                               false,
;;                              "landscape");
;;     wakuwaku.core.couponDialog();
;;

(def COUPON_EXPIRATION_SECURITY_MARGIN 15) ; seconds
(def WAKUWAKU_IMAGE_CACHE_SIZE 10)

(def client (atom nil)) ; Exposed methods will work on this.

(declare ClientProtocol)

(declare init)
(declare withInitCheck)
(declare swapClient!)

(defn ^:export activate
  "#### activate

   Turn-key integration.

   * Initialize the SDK.
   * Register a callback to get a coupon when nothing's left.
   * Fetch the first coupon.\n\n"
  [container
   companyId
   appId
   productionMode
   orientation]
  (init container companyId appId productionMode orientation WAKUWAKU_IMAGE_CACHE_SIZE))

(declare doInit)
(declare isWakuMpatible?)
(declare whenNoCouponLeft)
(declare fetch)

(defn ^:export init
  "#### init

   Initializes a SDK.

   - **container** DOM element where the coupon will be contained
   - **productionMode** true for production mode, false for test mode (fake coupons).
   - **orient** Desired coupon orientation landscape (default) or portrait.
   - **companyId** The unique string for the company using this SDK.
   - **appId** The unique string for the application using this SDK.
   - **bitmap_cache_size** How many coupon images at max can be loaded.\n\n"
  [container
   companyId
   appId
   productionMode
   orientation
   bitmap_cache_size]
  (let [_init (fn [] (doInit container
                             companyId
                             appId
                             productionMode
                             orientation
                             bitmap_cache_size
                             (fn [client_after_doinit]
                               (let [custom_client (whenNoCouponLeft client_after_doinit (fn [_] (fetch 1)))]
                                 (swap! client (fn [_] custom_client))
                                 (fetch 1)))))]
    (if (nil? container)
      (print "failed to initialize the WakuWaku sdk, context provided is nil.")
      (if productionMode
        (isWakuMpatible?
         (fn [waku_compatible]
           (if (waku_compatible)
             (do
               (print "Detected network compatible with WAKU WAKU. The SDK will serve coupons.")
               (_init))
             (print "This device is not running on a network served by WAKU WAKU. The SDK will not serve coupons."))))
        (do
          (print "Detected development mode. The SDK will serve coupons, EVEN IN UNSUPPORTED COUNTRIES.")
          (_init))))))

(declare doWhenNoCouponLeft)

(defn ^:export whenNoCouponLeft
  "#### whenNoCouponLeft
   Sets a callback to execute when all coupons are released.\n\n"
  [callback]
  (swapClient! (fn [client] (doWhenNoCouponLeft client callback))))

(declare doFetch)
(declare addCoupon)

(defn ^:export fetch
  "#### fetch
   Fetches *count* coupons.\n\n"
  [count]
  ((withInitCheck (fn [client]
                    (doFetch client
                             count
                             (fn [coupon] (swapClient! (fn [client] addCoupon client coupon))))))
   @client))

(declare takeCoupon)

(defn ^:export take
  "#### take
   Take a coupon if some prefetched one is still available.\n\n"
  []
  ((withInitCheck (fn [client] (takeCoupon client)))
   @client))

(declare releaseCoupon)
(declare checkCouponLeft!)

(defn ^:export release
  "#### release
   Releases a coupon.\n\n"
  [coupon]
  (swapClient! (fn [client] (releaseCoupon client coupon)))
  ((withInitCheck (fn [client] (checkCouponLeft! client)))
   @client))

(declare doCouponDialog)

(defn ^:export couponDialog
  "#### couponDialog

   Display a coupon inside the DOM container setup through #activate.\n\n"
  []
  (swapClient! (fn [client] (doCouponDialog client))))

;; ----
;; Some state management.
;;
;; We are in JS land so the STM semantics should not come into play!

(defn withInitCheck [f]
  (fn [client]
    (if (not (nil? client))
      (f client)
      (print "tried operation on nil client: " f))))

(defn swapClient! [f]
  (swap! client (withInitCheck f)))

;; ----
;; Protocol for Clients

(defprotocol ClientProtocol
  (addCoupon [this coupon])
  (takeCoupon [this coupon])
  (releaseCoupn [this coupon])
  (whenNoCouponLeft [this callback])
  (checkCouponLeft! [this]))

;; ### Private implementation
;;
;; Ideally we would move all of that in its own namespace eventually, but not now...

;; #### Core
;;

;; Note that in Clojure(Script) records are immutables, and accessible as usual maps. 
(defrecord Client
    [coupon_expiration_security_margin
     uuid2coupon
     mode
     core_url
     core_key
     company_id
     gid
     email
     orientation
     when_no_coupon_left
     image_cache]
  ClientProtocol
  (addCoupon [this coupon] (update-in this [:uuid2coupon (:uuid coupon)] (fn [_] coupon)))
  (takeCoupon [this coupon] (quote TODO))
  (releaseCoupon [this coupon] (quote TODO))
  (checkCouponLeft! [this] (when (= (count (:uuid2coupon this)) 0)
                             ((:when_no_coupon_left this) this)))
  (whenNoCouponLeft [this callback] (assoc this :when_no_coupon_left callback)))

(declare fetchCoupon)

(defn doFetch
  [client
   count
   on_fetch]
  "This function can fetch *count* coupons."
  (dotimes [n count]
    (fetchCoupon client
                 (-> js/UUID .create .toString)
                 (* 5 60)
                 (fn [coupon]
                   (let [client (assoc client :coupons (quote add coupon to set))] ; TODO here
                     (on_fetch client))))))

(defn doWhenNoCouponLeft
  [callback]
  "This functions will return a new client with the valid callback set."
  (assoc client :when_no_coupon_left callback))

(defn makeXmlHttpReq [callback_ok callback_fail]
  (let [req (js/XMLHttpRequest. )
        DONE 4]
    (aset req "onreadystatechange" (fn []
                                     (when (= (aget req "readyState") DONE)
                                       (if (= (aget req "status") 200)                                         
                                         (callback_ok (aget req "response"))
                                         (callback_fail (aget req "statusText"))))))
    
    req))

(defn asyncGet [url response_type callback]
  (let [req (makeXmlHttpReq callback
                            (fn [status_text] (print (str "request GET/" url  "failed with status " status_text))))]
    (.open req "GET" url)
    (aset req "responseType" response_type)
    (.send req)))

(defn asyncPost [url response_type payload callback]
  (let [req (makeXmlHttpReq callback
                            (fn [status_text] (print (str "request POST/" url  "failed with status " status_text))))]
    (.open req "POST" url)
    (.setRequestHeader req "Content-Type" "application/text;charset=UTF-8") ; we always send base64 encrypted stuff
    (aset req "responseType" response_type)
    (.send req payload)))

(declare loadCoreKey!)

(defn doInit
  "This function returns a valid wakuwaku client.

   Good code would isolate js/SimpleLRU with defprotocol&defrecord but bower returns only one js lru library and
   the abstraction cost seems higher than just using raw js calls. Especially when we convert this code from CLJS to JS."
  [container
   companyId
   appId
   productionMode
   orientation
   bitmap_cache_size
   when_done]
  (let [email (.get goog.net.cookies  "email")
        client (map->Client {:coupon_expiration_security_margin COUPON_EXPIRATION_SECURITY_MARGIN
                             :uuid2coupon {}
                             :mode (if productionMode "production" "test")
                             :core_url (if productionMode "https://core.waku-waku.ne.jp" "http://uat.waku-waku.ne.jp:2929")
                             :core_key ""
                             :company_id companyId
                             :gid appId
                             :email (or email "")
                             :orientation orientation
                             :when_no_coupon_left (fn [client] (print (str "do nothing when no coupon left, with client: " client)))
                             :image_cache (js/SimpleLRU. bitmap_cache_size)})]
    (loadCoreKey! client(fn [client] (when_done client)))))

(defn loadCoreKey!
  [client callback]
  (asyncGet (str (:core_url client) "/1.0/public-key")
            "text"
            (fn [raw_key]
              (let [raw_key_noheader (string/replace raw_key "-----BEGIN PUBLIC KEY-----" "")
                    b64key (string/replace raw_key_noheader "-----END PUBLIC KEY-----" "")
                    hexkey (js/Base64.decode b64key)
                    parsed_key (print "b64key->" b64key "\n\nhexkey->" hexkey) ; TODO
                    client (assoc client :core_key parsed_key)]
                (callback client)))))

;; Record holding data related to coupons!
(defrecord Coupon [url code uuid release_ms lifetime impressions])

(defn createCoupon!
  "Creates a Coupon, loads its image in the background and schedule its lifetime extension."
  [url code uuid lifetime extra_actions]
  (let [coupon (map->Coupon {:url url
                             :code code
                             :uuid uuid
                             :release_ms (quote TODO)
                             :impressions extra_actions})]
    (quote TODO
           load image in the background
           schedule lifetime extend)))

(defn getImage
  "This function returns a JS Image object!"
  [coupon cache]
  nil)

(defn fetchCoupon
  "This function will fetch a coupon identified by a *ucid* string. The coupon
   is available for *lifetime* seconds.

   - **ucid** Unique coupon ID
   - **param** lifetime coupon reservation time, in seconds. Max is 5 min.
   
   Returns a coupon, if any available. It returns nil if no coupon."
  [client ucid lifetime callback]
  (let [payload {"uuid" ucid
                 "lifetime" lifetime
                 "uaid" (:gid client)
                 "myid" (:company_id client)
                 "orientation" (:orientation client)}
        encrypted_payload (quote TODO)
        url (str (:core_url client) "/1.0/coupon/take")]
    (quote 
     TODO_post
     TODO_checkresponsesize
     TODO_parseresponsejson
     TODO_parsestuff_and_make_a_coupon)))

(defn registerImpression
  "When an impression has been done, and the corresponding advertiser
   requires to mark it with an API call. This method just does that
   call.
   
   **todo** This method should not ignore the result of the call.
     Right now, we do not have use for it, but some error
     tracking could be useful in the future."
  [urls]
  nil)

(defn publicCoreEncrypt
  [client in]
  nil)

(defn toEmail
  [client coupon]
  (let [payload {"type" "email"
                 "uid" (:email client)
                 "gid" (:gid client)
                 "uuid" (:uuid coupon)
                 "code" (:code coupon)}
        url (str (:core_url client) "/1.0/coupon" (:uuid client) "/bind")
        encrypted_payload (quote TODO)]
    (asyncPost url encrypted_payload "" (fn [_] (print "post email request sent!!")))))

(defn isWakuMpatible?
  [callback]
  (asyncGet "http://freegeoip.net/json/"
             "json"
             (fn [resp]
               (if (= (:country-code resp) "JP")
                 (callback true)
                 (callback false)))))

(defn getCountryCode
  []
  nil)

;; #### UI
;;
;; This too should be in its own namespace
;;

(defn showCouponDialog
  [coupon]
  nil)

(defn sendMailEvent
  [event]
  nil)

(defn closeDialogEvent
  [event]
  nil)

(if-not (ws-repl/alive?)
  (ws-repl/connect "ws://localhost:9001" :verbose true))
