(defproject wakuwaku-js-sdk "0.1.0-dev"
  :description "Javascript SDK for WakuWaku. Demo version."
  :url "https://bitbucket.org/freakhill/wakuwaku-js-sdk"
  
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/clojurescript "0.0-2227"]
                 [prismatic/dommy "0.1.2"]
                 [weasel "0.2.1"]]

  :source-paths ["src"]
  
  :profiles {:dev {:dependencies [[com.cemerick/piggieback "0.1.3"]]
                   :plugins [[lein-cljsbuild "1.0.3"]
                             [lein-marginalia "0.7.1"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                   :cljsbuild {:builds [{:id "wakuwaku"
                        :source-paths ["src"]
                        :notify-command ["./on_notify_regen_doc.sh"]
                        :compiler {:output-to "out_none/wakuwaku.js"
                                   :output-dir "out_none"
                                   :optimizations :none
                                   :source-map "out_none/wakuwaku.js.map"}}
                       {:id "wakuwaku-optimized"
                        :source-paths ["src"]
                        :compiler {:output-to "out_advanced/wakuwaku.js"
                                   :output-dir "out_advanced"
                                   :optimizations :advanced
                                   :source-map "out_advanced/wakuwaku.js.map"}}]}}})
