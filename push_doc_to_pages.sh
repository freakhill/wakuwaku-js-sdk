#!/usr/bin/env bash

set -e # crash on error
set -v # verbose

cleanup() {
    git checkout master
    git branch
}

trap cleanup 0

# only work on my env...

git checkout pages
git checkout origin/master .gitignore
git checkout origin/master docs/uberdoc.html
git commit -am "update docs"
git push pages HEAD:master

#############################
# .git/config setup as such #
#############################
cat >/dev/null <<EOF
[remote "origin"]
	url = git@bitbucket.org:freakhill/wakuwaku-js-sdk.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
[remote "pages"]
	url = git@bitbucket.org:freakhill/freakhill.bitbucket.org.git
	fetch = +refs/heads/*:refs/remotes/pages/*
[branch "pages"]
	remote = pages
	merge = refs/heads/master
EOF
