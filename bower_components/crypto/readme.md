'crypto' nodejs core module browserify-ied with `--standalone crypto`. Should support all module systems (commonjs, AMD & `window.crypto`) - check browserify docs.

From [node2web](http://github.com/anodynos/node2web) collection,
should/will be exposed as 'crypto' to [bower](http://bower.io) for *browser* usage.

browserify version: '3.24.10', build date 'Sun Feb 02 2014 23:17:40 GMT+0200 (EET)'