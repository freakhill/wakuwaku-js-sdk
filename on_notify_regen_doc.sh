#!/usr/bin/env bash

echo "$*"
lein marg
if [[ `uname` == 'Darwin' ]]
then
    open -a safari docs/uberdoc.html
    # safari because it refreshes instead of opening a new window
fi
